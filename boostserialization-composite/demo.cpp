#include <fstream>

// include headers that implement a archive in simple text format
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>

/////////////////////////////////////////////////////////////
// gps coordinate
//
// illustrates serialization for a simple type
//
class gps_position
{
private:
	friend class boost::serialization::access;
	// When the class Archive corresponds to an output archive, the
	// & operator is defined similar to <<.  Likewise, when the class Archive
	// is a type of input archive the & operator is defined similar to >>.
	template<class Archive>	void serialize(Archive & ar, const unsigned int version)
	{
		ar & degrees;
		ar & minutes;
		ar & seconds;
	}
	int degrees;
	int minutes;
	float seconds;
public:
	gps_position(){};
	gps_position(int d, int m, float s) :
		degrees(d), minutes(m), seconds(s)
	{}
};

class bus_stop
{
	friend class boost::serialization::access;
	template<class Archive>	void serialize(Archive & ar, const unsigned int version)
	{
		ar & latitude;
		ar & longitude;
	}
	gps_position latitude;
	gps_position longitude;
public:
	bus_stop(const gps_position & lat_, const gps_position & long_) : latitude(lat_), longitude(long_)
	{}
public:
	bus_stop(){}
	// See item # 14 in Effective C++ by Scott Meyers.
	// re non-virtual destructors in base classes.
	virtual ~bus_stop(){}
};

int main()
{
	std::ofstream ofs("filename");

	const bus_stop stop(gps_position(10, 10, 10), gps_position(11, 11, 11));

	{
		boost::archive::text_oarchive oa(ofs);

		oa << stop;
	}

	bus_stop bus_stop_copy;

	{
		std::ifstream ifs("filename");

		boost::archive::text_iarchive ia(ifs);

		ia >> bus_stop_copy;
	}

	return 0;
}